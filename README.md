
## HOW TO RUN

  - Run docker-compose up in this project directory
  - Application should be accessible on localhost:6868

### RESTful Spring Boot app

  #### Swagger documentation
  - localhost:8081/v2/api-docs
  - localhost:8081/swagger-ui.html#/
  
## Docker containers accessible via docker hub: 
  - https://hub.docker.com/r/limak360/oizpi
  - https://hub.docker.com/r/fgfmnycz/oizpi
