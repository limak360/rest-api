package com.kamiljacko.restapi.shop;

import com.kamiljacko.restapi.CommonMapper;
import com.kamiljacko.restapi.product.ProductService;
import com.kamiljacko.restapi.region.Region;
import com.kamiljacko.restapi.region.RegionRepository;
import com.kamiljacko.restapi.stock.Stock;
import com.kamiljacko.restapi.stock.StockRepository;
import com.kamiljacko.restapi.web.dto.MinimalShopDto;
import com.kamiljacko.restapi.web.dto.ProductFromGivenShopDto;
import com.kamiljacko.restapi.web.dto.ShopDto;
import com.kamiljacko.restapi.web.dto.StockDto;

import java.util.List;
import java.util.Optional;
import java.util.stream.Collectors;
import java.util.stream.StreamSupport;

@org.springframework.stereotype.Service
public class ShopServiceImpl implements ShopService {

    private final ProductService productService;
    private final ShopRepository shopRepository;
    private final RegionRepository regionRepository;
    private final StockRepository stockRepository;

    public ShopServiceImpl(final ShopRepository shopRepository,
                           final ProductService productService,
                           final RegionRepository regionRepository,
                           final StockRepository stockRepository) {
        this.shopRepository = shopRepository;
        this.productService = productService;
        this.regionRepository = regionRepository;
        this.stockRepository = stockRepository;
    }

    public List<StockDto> addProductsToGivenShop(final ShopDto shopDto) {
        final Optional<Region> region = getRegionByRegionName(shopDto.getRegionName());
        final Integer regionId = region.map(Region::getRegionId).orElse(0);

        final Shop shopToSave = new Shop(shopDto.getName(), regionId);
        final Integer shopId = shopRepository.save(shopToSave).getShopId();

        final List<Stock> stocksMain = productService.getAllFullProductsFromMainRegion().stream()
                .map(productDto -> new Stock(productDto, shopId))
                .collect(Collectors.toList());

        final String regionName = region.map(Region::getName).orElse("");
        final List<Stock> stocksRegional = productService.getAllFullProductsFromGivenRegion(regionName).stream()
                .map(productDto -> new Stock(productDto, shopId))
                .collect(Collectors.toList());

        stocksMain.addAll(stocksRegional);
        final Iterable<Stock> iterable = stockRepository.saveAll(stocksMain);

        return StreamSupport.stream(iterable.spliterator(), false)
                .map(CommonMapper::mapStockToDto)
                .collect(Collectors.toList());
    }

    @Override
    public List<ProductFromGivenShopDto> getAllProductsFromGivenShop(final String shopName) {
        return shopRepository.getAllProductsFromGivenShop(shopName).stream()
                .map(CommonMapper::mapToDto)
                .collect(Collectors.toList());
    }

    @Override
    public List<MinimalShopDto> getAllShopsInGivenRegion(final String regionName) {
        final Optional<Region> region = getRegionByRegionName(regionName);
        final Integer regionId = region.map(Region::getRegionId).orElse(0);

        return shopRepository.getAllShopsFromGivenRegion(regionId).stream()
                .map(s -> new MinimalShopDto(s.getShopId(), s.getName()))
                .collect(Collectors.toList());
    }

    private Optional<Region> getRegionByRegionName(final String regionName) {
        return Optional.ofNullable(regionRepository.findRegionByRegionName(regionName));
    }
}
