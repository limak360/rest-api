package com.kamiljacko.restapi.shop;

import com.kamiljacko.restapi.region.Region;
import com.kamiljacko.restapi.stock.Stock;
import com.kamiljacko.restapi.user.User;

import javax.persistence.Column;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.OneToMany;
import javax.persistence.Table;
import java.util.List;
import java.util.Objects;
import java.util.Set;

@javax.persistence.Entity
@Table(name = "shops")
public class Shop {

    @Id
    @Column(name = "shopid")
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Integer shopId;

    private String name;

    @OneToMany(mappedBy = "shop")
    private Set<Stock> stocks;

    @ManyToOne
    @JoinColumn(name = "regionid")
    private Region region;

    @OneToMany(mappedBy = "shop")
    private List<User> user;

    public Shop() {
    }

    public Shop(Integer shopId) {
        this.shopId = shopId;
    }

    public Shop(String name, Integer regionId) {
        this.name = name;
        this.region = new Region(regionId);
    }

    public Integer getShopId() {
        return shopId;
    }

    public void setShopId(Integer shopId) {
        this.shopId = shopId;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public Set<Stock> getStocks() {
        return stocks;
    }

    public void setStocks(Set<Stock> stocks) {
        this.stocks = stocks;
    }

    public Region getRegion() {
        return region;
    }

    public Integer getRegionId() {
        return region.getRegionId();
    }

    public void setRegion(Region region) {
        this.region = region;
    }

    public List<User> getUser() {
        return user;
    }

    public void setUser(List<User> user) {
        this.user = user;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        Shop shop = (Shop) o;
        return Objects.equals(shopId, shop.shopId)
                && Objects.equals(name, shop.name)
                && Objects.equals(stocks, shop.stocks)
                && Objects.equals(region, shop.region)
                && Objects.equals(user, shop.user);
    }

    @Override
    public int hashCode() {
        return Objects.hash(shopId, name, stocks, region, user);
    }

    @Override
    public String toString() {
        return "Shop{" +
                "shopId=" + shopId +
                ", name='" + name + '\'' +
                ", stocks=" + stocks +
                ", region=" + region +
                ", user=" + user +
                '}';
    }
}
