package com.kamiljacko.restapi.shop;

import com.kamiljacko.restapi.product.Category;
import com.kamiljacko.restapi.product.Product;
import com.kamiljacko.restapi.region.Region;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.CrudRepository;

import java.util.List;


public interface ShopRepository extends CrudRepository<Shop, Integer> {

    String COMMON_PRODUCT_SELECT = "SELECT s.name AS name, s.shopId As shopId, s.region AS shopRegionId, " +
            "st.product AS product, st.amount AS amount, st.price AS price, st.discountPrice AS discountPrice, st.isDiscounted AS isDiscounted, " +
            "p.name AS productName, p.region AS productRegionId, p.category AS productCategoryId, p.description AS productDescription, p.imgUrl AS imgUrl " +
            "FROM Stock st JOIN Shop s ON st.shop = s.shopId JOIN Product p ON st.product = p.productId ";

    Shop save(final Shop shop);

    @Query(COMMON_PRODUCT_SELECT + "WHERE s.name =:shopName")
    List<FullDao> getAllProductsFromGivenShop(final String shopName);

    @Query(COMMON_PRODUCT_SELECT + "WHERE s.name =:shopName AND p.productId =:productId")
    FullDao getProductFromGivenShop(final String shopName, final Integer productId);

    @Query("SELECT shopId FROM Shop WHERE name=:shopName")
    Integer getShopIdByName(final String shopName);

    @Query("SELECT shopId AS shopId, name AS name FROM Shop WHERE regionId=:regionId")
    List<FullDao> getAllShopsFromGivenRegion(final Integer regionId);

    interface FullDao {
        String getName();

        Integer getShopId();

        Region getShopRegionId();

        Product getProduct();

        Integer getAmount();

        String getPrice();

        String getDiscountPrice();

        Integer getIsDiscounted();

        String getProductName();

        Region getProductRegionId();

        Category getProductCategoryId();

        String getProductDescription();

        String getImgUrl();
    }
}
