package com.kamiljacko.restapi.shop;

import com.kamiljacko.restapi.web.dto.ProductFromGivenShopDto;
import com.kamiljacko.restapi.web.dto.ShopDto;
import com.kamiljacko.restapi.web.dto.MinimalShopDto;
import com.kamiljacko.restapi.web.dto.StockDto;

import java.util.List;

public interface ShopService {

    List<StockDto> addProductsToGivenShop(final ShopDto shopDto);

    List<ProductFromGivenShopDto> getAllProductsFromGivenShop(final String shopName);

    List<MinimalShopDto> getAllShopsInGivenRegion(final String regionName);
}
