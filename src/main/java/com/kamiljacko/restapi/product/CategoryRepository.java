package com.kamiljacko.restapi.product;

import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.CrudRepository;

public interface CategoryRepository extends CrudRepository<Category, Integer> {

    @Query("FROM Category Where name=:categoryName")
    Category findCategoryByCategoryName(final String categoryName);
}
