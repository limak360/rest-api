package com.kamiljacko.restapi.product;


import org.springframework.data.jpa.repository.Modifying;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.CrudRepository;

import javax.transaction.Transactional;
import java.util.List;

@org.springframework.stereotype.Repository
@Transactional
public interface ProductRepository extends CrudRepository<Product, Integer> {

    // mozna dodac tylko produkty regionalne
    @Query("SELECT st.product AS product, st.amount AS amount, st.price AS price, st.discountPrice AS discountPrice, st.isDiscounted AS isDiscounted " +
            "FROM Product p JOIN Stock st ON p.productId = st.product WHERE p.region=1 AND st.shop=1")
    List<ProductMainDao> getAllFullProductsFromMainRegion();

    // Problem https://discourse.hibernate.org/t/parameter-value-did-not-match-expected-type-even-thought-it-matches/2353/10
    @Query(value = "SELECT st.productid AS productId, p.name AS productName, st.amount AS amount, st.price AS price," +
            " st.discount_price AS discountPrice, st.is_discounted AS isDiscounted " +
            "FROM products p JOIN stocks st ON p.productid = st.productid WHERE p.regionid=:regionId", nativeQuery = true)
    List<ProductRegionalDao> getAllFullProductsFromGivenRegion(final Integer regionId);

    @Query("FROM Product WHERE regionId=:regionId")
    List<Product> getAllProductsFromGivenRegion(final Integer regionId);

    @Query("FROM Product")
    List<Product> getAllProductsFromGivenRegion();

    @Modifying
    @Query(value = "DELETE stocks FROM stocks " +
            "JOIN shops ON stocks.shopid = shops.shopid JOIN products ON stocks.productid = products.productid " +
            "WHERE products.name=:productName AND shops.name =:shopName", nativeQuery = true)
    void deleteProductFromShop(final String productName, final String shopName);

    interface ProductMainDao {
        Product getProduct();

        Integer getAmount();

        String getPrice();

        String getDiscountPrice();

        Integer getIsDiscounted();
    }

    interface ProductRegionalDao {
        Integer getProductId();

        String getProductName();

        Integer getAmount();

        String getPrice();

        String getDiscountPrice();

        Integer getIsDiscounted();
    }
}
