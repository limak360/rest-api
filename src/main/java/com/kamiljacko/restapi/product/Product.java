package com.kamiljacko.restapi.product;

import com.kamiljacko.restapi.region.Region;
import com.kamiljacko.restapi.stock.Stock;

import javax.persistence.Column;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.OneToMany;
import javax.persistence.Table;
import java.util.Objects;
import java.util.Set;

@javax.persistence.Entity
@Table(name = "products")
public class Product {

    @Id
    @Column(name = "productid")
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Integer productId;

    private String name;
    private String description;

    @OneToMany(mappedBy = "product")
    private Set<Stock> stocks;

    @ManyToOne
    @JoinColumn(name = "regionid")
    private Region region;

    @ManyToOne
    @JoinColumn(name = "categoryid")
    private Category category;

    @Column(name = "imgURL")
    private String imgUrl;

    public Product() {
    }

    public Product(String name) {
        this.name = name;
    }

    public Product(Integer productId) {
        this.productId = productId;
    }

    public Product(String productName, Integer productId) {
        this.name = productName;
        this.productId = productId;
    }

    public Product(String productName, Integer productId, String productDescription, String imgUrl, Integer categoryId) {
        this.name = productName;
        this.productId = productId;
        this.description = productDescription;
        this.imgUrl = imgUrl;
        this.category = new Category(categoryId);
    }

    public Integer getProductId() {
        return productId;
    }

    public void setProductId(Integer productId) {
        this.productId = productId;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public Set<Stock> getStocks() {
        return stocks;
    }

    public void setStocks(Set<Stock> stocks) {
        this.stocks = stocks;
    }

    public Region getRegion() {
        return region;
    }

    public void setRegion(Region region) {
        this.region = region;
    }

    public String getImgUrl() {
        return imgUrl;
    }

    public void setImgUrl(String imgUrl) {
        this.imgUrl = imgUrl;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    public Category getCategory() {
        return category;
    }

    public void setCategory(Category category) {
        this.category = category;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        Product product = (Product) o;
        return Objects.equals(productId, product.productId) &&
                Objects.equals(name, product.name) &&
                Objects.equals(description, product.description) &&
                Objects.equals(stocks, product.stocks) &&
                Objects.equals(region, product.region) &&
                Objects.equals(category, product.category) &&
                Objects.equals(imgUrl, product.imgUrl);
    }

    @Override
    public int hashCode() {
        return Objects.hash(productId, name, description, stocks, region, category, imgUrl);
    }

    @Override
    public String toString() {
        return "Product{" +
                "productId=" + productId +
                ", name='" + name + '\'' +
                ", description='" + description + '\'' +
                ", stocks=" + stocks +
                ", region=" + region +
                ", category=" + category +
                ", imgUrl='" + imgUrl + '\'' +
                '}';
    }
}
