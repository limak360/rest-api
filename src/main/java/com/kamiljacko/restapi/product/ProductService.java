package com.kamiljacko.restapi.product;

import com.kamiljacko.restapi.web.dto.ProductDto;

import java.util.List;

public interface ProductService {

    List<ProductDto> getAllFullProductsFromMainRegion();

    List<ProductDto> getAllFullProductsFromGivenRegion(final String regionName);

    List<ProductDto> getAllProductsFromGivenRegion(final String regionName);

    void deleteProductFromShop(final String productName, final String shopName);
}
