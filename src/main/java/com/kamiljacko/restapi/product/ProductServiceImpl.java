package com.kamiljacko.restapi.product;

import com.kamiljacko.restapi.region.Region;
import com.kamiljacko.restapi.region.RegionRepository;
import com.kamiljacko.restapi.web.dto.ProductDto;

import java.util.ArrayList;
import java.util.List;
import java.util.Optional;
import java.util.stream.Collectors;

@org.springframework.stereotype.Service
public class ProductServiceImpl implements ProductService {

    private final ProductRepository productRepository;
    private final RegionRepository regionRepository;

    public ProductServiceImpl(final ProductRepository productRepository, final RegionRepository regionRepository) {
        this.productRepository = productRepository;
        this.regionRepository = regionRepository;
    }

    public List<ProductDto> getAllFullProductsFromMainRegion() {
        return productRepository.getAllFullProductsFromMainRegion().stream()
                .map(this::mapToDtoMain)
                .collect(Collectors.toList());
    }

    public List<ProductDto> getAllFullProductsFromGivenRegion(final String regionName) {
        final Optional<Region> region = Optional.ofNullable(regionRepository.findRegionByRegionName(regionName));
        return region.map(value -> productRepository.getAllFullProductsFromGivenRegion(value.getRegionId()).stream()
                .map(this::mapToDtoRegional)
                .collect(Collectors.toList())).orElseGet(ArrayList::new);
    }

    @Override
    public List<ProductDto> getAllProductsFromGivenRegion(final String regionName) {
        final Optional<Region> region = Optional.ofNullable(regionRepository.findRegionByRegionName(regionName));
        return region.map(value -> productRepository.getAllProductsFromGivenRegion(value.getRegionId()).stream()
                .map(this::mapToDto)
                .collect(Collectors.toList())).orElseGet(ArrayList::new);
    }

    @Override
    public void deleteProductFromShop(final String productName, final String shopName) {
        productRepository.deleteProductFromShop(productName, shopName);
    }

    private ProductDto mapToDto(Product product) {
        final ProductDto productDto = new ProductDto();
        productDto.setProductId(product.getProductId());
        productDto.setProductName(product.getName());
        productDto.setRegionId(product.getRegion().getRegionId());
        productDto.setRegionName(product.getRegion().getName());
        productDto.setCategoryId(product.getCategory().getCategoryId());
        productDto.setCategoryName(product.getCategory().getName());
        productDto.setProductDescription(product.getDescription());
        productDto.setImgUrl(product.getImgUrl());
        return productDto;
    }

    private ProductDto mapToDtoRegional(final ProductRepository.ProductRegionalDao productRegionalDao) {
        final ProductDto productDto = new ProductDto();
        productDto.setProductId(productRegionalDao.getProductId());
        productDto.setProductName(productRegionalDao.getProductName());
        productDto.setAmount(productRegionalDao.getAmount());
        productDto.setPrice(productRegionalDao.getPrice());
        productDto.setDiscountPrice(productRegionalDao.getDiscountPrice());
        productDto.setIsDiscounted(productRegionalDao.getIsDiscounted());
        return productDto;
    }

    private ProductDto mapToDtoMain(final ProductRepository.ProductMainDao productMainDao) {
        final ProductDto productDto = new ProductDto();
        productDto.setProductId(productMainDao.getProduct().getProductId());
        productDto.setProductName(productMainDao.getProduct().getName());
        productDto.setAmount(productMainDao.getAmount());
        productDto.setPrice(productMainDao.getPrice());
        productDto.setDiscountPrice(productMainDao.getDiscountPrice());
        productDto.setIsDiscounted(productMainDao.getIsDiscounted());
        return productDto;
    }
}
