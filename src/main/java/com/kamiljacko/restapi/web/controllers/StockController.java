package com.kamiljacko.restapi.web.controllers;

import com.kamiljacko.restapi.stock.StockService;
import com.kamiljacko.restapi.web.dto.MinimalProductDto;
import com.kamiljacko.restapi.web.dto.ProductDto;
import com.kamiljacko.restapi.web.dto.StockDto;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import java.util.List;

@RestController
@RequestMapping("/stocks")
public class StockController {

    private final StockService stockService;

    public StockController(final StockService stockService) {
        this.stockService = stockService;
    }

    @GetMapping("/products")
    public ResponseEntity<List<MinimalProductDto>> getAllProducts() {
        final HttpStatus status = HttpStatus.OK;
        final List<MinimalProductDto> allProducts = stockService.getAllProducts();
        return new ResponseEntity<>(allProducts, status);
    }

    @PostMapping("/{shopName}")
    public ResponseEntity<StockDto> addRegionalProductToGivenShop(@PathVariable final String shopName, @RequestBody final ProductDto productDto) {
        final HttpStatus status = HttpStatus.CREATED;
        final StockDto stock = stockService.addRegionalProductToGivenShop(shopName, productDto);
        return new ResponseEntity<>(stock, status);
    }

    @PutMapping("/{shopName}")
    public ResponseEntity<StockDto> updateProduct(@PathVariable final String shopName, @RequestBody final ProductDto updateDto) {
        final HttpStatus status = HttpStatus.OK;
        stockService.updateProduct(shopName, updateDto);
        return new ResponseEntity<>(status);
    }
}