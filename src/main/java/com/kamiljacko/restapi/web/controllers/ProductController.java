package com.kamiljacko.restapi.web.controllers;


import com.kamiljacko.restapi.product.Product;
import com.kamiljacko.restapi.product.ProductService;
import com.kamiljacko.restapi.web.dto.ProductDto;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import java.util.List;

@RestController
@RequestMapping("/products")
public class ProductController {

    private final ProductService productService;

    public ProductController(final ProductService productService) {
        this.productService = productService;
    }

    @GetMapping("/{regionName}")
    public ResponseEntity<List<ProductDto>> getAllProductsFromGivenRegion(@PathVariable final String regionName) {
        final HttpStatus status = HttpStatus.OK;
        final List<ProductDto> allProductsFromGivenRegion = productService.getAllProductsFromGivenRegion(regionName);
        return new ResponseEntity<>(allProductsFromGivenRegion, status);
    }

    @DeleteMapping("/{productName}/{shopName}")
    public ResponseEntity<Product> deleteProduct(@PathVariable final String productName, @PathVariable final String shopName) {
        final HttpStatus status = HttpStatus.OK;
        productService.deleteProductFromShop(productName, shopName);
        return new ResponseEntity<>(status);
    }
}
