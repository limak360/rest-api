package com.kamiljacko.restapi.web.controllers;

import com.kamiljacko.restapi.product.Product;
import com.kamiljacko.restapi.user.UserService;
import com.kamiljacko.restapi.web.dto.StockDto;
import com.kamiljacko.restapi.web.dto.UserDto;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import java.util.List;

@RestController
@RequestMapping("/user")
public class UserController {

    private final UserService userService;

    public UserController(final UserService userService) {
        this.userService = userService;
    }

    @GetMapping
    public ResponseEntity<List<UserDto>> getAllUsers() {
        final HttpStatus status = HttpStatus.OK;
        final List<UserDto> allUsers = userService.getAllUsers();
        return new ResponseEntity<>(allUsers, status);
    }

    @PostMapping
    public ResponseEntity<UserDto> addUser(@RequestBody final UserDto userDto) {
        final HttpStatus status = HttpStatus.OK;
        final UserDto user = userService.addUser(userDto);
        return new ResponseEntity<>(user, status);
    }

    @PutMapping("/update")
    public ResponseEntity<StockDto> updateProduct(@RequestBody final UserDto userDto) {
        final HttpStatus status = HttpStatus.OK;
        userService.updateUser(userDto);
        return new ResponseEntity<>(status);
    }

    @DeleteMapping("/{userId}")
    public ResponseEntity<Product> deleteUser(@PathVariable final Integer userId) {
        final HttpStatus status = HttpStatus.OK;
        userService.deleteUser(userId);
        return new ResponseEntity<>(status);
    }
}
