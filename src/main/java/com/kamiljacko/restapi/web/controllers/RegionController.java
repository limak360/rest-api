package com.kamiljacko.restapi.web.controllers;


import com.kamiljacko.restapi.product.Product;
import com.kamiljacko.restapi.region.RegionService;
import com.kamiljacko.restapi.web.dto.RegionDto;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import java.util.List;

@RestController
@RequestMapping("/regions")
public class RegionController {

    private final RegionService regionService;

    public RegionController(final RegionService regionService) {
        this.regionService = regionService;
    }

    @GetMapping
    public ResponseEntity<List<RegionDto>> getAllRegions() {
        final HttpStatus status = HttpStatus.OK;
        final List<RegionDto> allRegions = regionService.getAllRegions();
        return new ResponseEntity<>(allRegions, status);
    }

    @PostMapping
    public ResponseEntity<RegionDto> addRegion(@RequestBody final RegionDto regionDto) {
        final HttpStatus status = HttpStatus.OK;
        final RegionDto region = regionService.addRegion(regionDto);
        return new ResponseEntity<>(region, status);
    }

    @DeleteMapping("/{regionName}")
    public ResponseEntity<Product> deleteRegion(@PathVariable final String regionName) {
        final HttpStatus status = HttpStatus.OK;
        regionService.deleteRegion(regionName);
        return new ResponseEntity<>(status);
    }

}
