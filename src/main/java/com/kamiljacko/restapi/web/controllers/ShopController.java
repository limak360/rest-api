package com.kamiljacko.restapi.web.controllers;

import com.kamiljacko.restapi.shop.ShopService;
import com.kamiljacko.restapi.web.dto.MinimalShopDto;
import com.kamiljacko.restapi.web.dto.ProductFromGivenShopDto;
import com.kamiljacko.restapi.web.dto.ShopDto;
import com.kamiljacko.restapi.web.dto.StockDto;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import java.util.List;

@RestController
@RequestMapping("/shops")
public class ShopController {

    private final ShopService shopService;

    public ShopController(final ShopService shopService) {
        this.shopService = shopService;
    }

    @GetMapping("/products/{shopName}")
    public ResponseEntity<List<ProductFromGivenShopDto>> getAllProductsFromGivenShop(@PathVariable final String shopName) {
        final HttpStatus status = HttpStatus.OK;
        final List<ProductFromGivenShopDto> shopProductDto = shopService.getAllProductsFromGivenShop(shopName);
        return new ResponseEntity<>(shopProductDto, status);
    }

    @GetMapping("/{regionName}")
    public ResponseEntity<List<MinimalShopDto>> getAllShopsInGivenRegion(@PathVariable final String regionName) {
        final HttpStatus status = HttpStatus.OK;
        final List<MinimalShopDto> shopDto = shopService.getAllShopsInGivenRegion(regionName);
        return new ResponseEntity<>(shopDto, status);
    }

    @PostMapping
    public ResponseEntity<List<StockDto>> addProductsToGivenShop(@RequestBody final ShopDto shopDto) {
        final HttpStatus status = HttpStatus.CREATED;
        final List<StockDto> productDtos = shopService.addProductsToGivenShop(shopDto);
        return new ResponseEntity<>(productDtos, status);
    }
}
