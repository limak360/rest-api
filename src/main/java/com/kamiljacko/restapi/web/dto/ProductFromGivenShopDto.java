package com.kamiljacko.restapi.web.dto;

public class ProductFromGivenShopDto {

    private Integer shopId;
    private Integer shopRegionId;
    private Integer productId;
    private String name;
    private Integer amount;
    private String price;
    private String discountPrice;
    private Integer isDiscounted;
    private String productName;
    private Integer productRegionId;
    private Integer productCategoryId;
    private String productDescription;
    private String imgUrl;

    public ProductFromGivenShopDto() {
    }

    public Integer getShopId() {
        return shopId;
    }

    public void setShopId(Integer shopId) {
        this.shopId = shopId;
    }

    public Integer getShopRegionId() {
        return shopRegionId;
    }

    public void setShopRegionId(Integer shopRegionId) {
        this.shopRegionId = shopRegionId;
    }

    public Integer getProductId() {
        return productId;
    }

    public void setProductId(Integer productId) {
        this.productId = productId;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public Integer getAmount() {
        return amount;
    }

    public void setAmount(Integer amount) {
        this.amount = amount;
    }

    public String getPrice() {
        return price;
    }

    public void setPrice(String price) {
        this.price = price;
    }

    public String getDiscountPrice() {
        return discountPrice;
    }

    public void setDiscountPrice(String discountPrice) {
        this.discountPrice = discountPrice;
    }

    public Integer getIsDiscounted() {
        return isDiscounted;
    }

    public void setIsDiscounted(Integer isDiscounted) {
        this.isDiscounted = isDiscounted;
    }

    public String getProductName() {
        return productName;
    }

    public void setProductName(String productName) {
        this.productName = productName;
    }

    public Integer getProductRegionId() {
        return productRegionId;
    }

    public void setProductRegionId(Integer productRegionId) {
        this.productRegionId = productRegionId;
    }

    public Integer getProductCategoryId() {
        return productCategoryId;
    }

    public void setProductCategoryId(Integer productCategoryId) {
        this.productCategoryId = productCategoryId;
    }

    public String getProductDescription() {
        return productDescription;
    }

    public void setProductDescription(String productDescription) {
        this.productDescription = productDescription;
    }

    public String getImgUrl() {
        return imgUrl;
    }

    public void setImgUrl(String imgUrl) {
        this.imgUrl = imgUrl;
    }

}