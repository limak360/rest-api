package com.kamiljacko.restapi.user;

import org.springframework.data.jpa.repository.Modifying;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.CrudRepository;

import javax.transaction.Transactional;

@Transactional
public interface UserRepository extends CrudRepository<User, Integer> {
    @Modifying
    @Query(value = "UPDATE users u " +
            "SET u.username =:username, u.password =:password, u.roleid =:roleId, u.regionid =:regionId, u.shopid =:shopId " +
            "WHERE u.userid =:userId", nativeQuery = true)
    void updateUser(final String username, final String password, final Integer roleId, final Integer regionId, final Integer shopId, final Integer userId);
}
