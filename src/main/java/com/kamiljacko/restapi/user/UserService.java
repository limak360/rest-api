package com.kamiljacko.restapi.user;

import com.kamiljacko.restapi.web.dto.UserDto;

import java.util.List;

public interface UserService {

    List<UserDto> getAllUsers();

    UserDto addUser(final UserDto userDto);

    void deleteUser(final Integer userId);

    void updateUser(final UserDto userDto);
}
