package com.kamiljacko.restapi.user;

import com.kamiljacko.restapi.region.Region;
import com.kamiljacko.restapi.shop.Shop;
import com.kamiljacko.restapi.web.dto.UserDto;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.Table;
import java.util.Objects;

@Entity
@Table(name = "users")
public class User {

    @Id
    @Column(name = "userid")
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Integer userId;

    private String username;
    private String password;

    @ManyToOne
    @JoinColumn(name = "regionid")
    private Region region;

    @ManyToOne
    @JoinColumn(name = "shopid")
    private Shop shop;

    @ManyToOne
    @JoinColumn(name = "roleid")
    private Role role;

    public User() {
    }

    public User(UserDto userDto) {
        this.userId = userDto.getUserId();
        this.username = userDto.getUsername();
        this.password = userDto.getPassword();
        this.role = new Role(userDto.getRoleId());
        this.region = new Region(userDto.getRegionId());
        this.shop = new Shop(userDto.getShopId());
    }

    public Integer getUserId() {
        return userId;
    }

    public void setUserId(Integer userId) {
        this.userId = userId;
    }

    public String getUsername() {
        return username;
    }

    public void setUsername(String username) {
        this.username = username;
    }

    public String getPassword() {
        return password;
    }

    public void setPassword(String password) {
        this.password = password;
    }

    public Region getRegion() {
        return region;
    }

    public void setRegion(Region region) {
        this.region = region;
    }

    public Shop getShop() {
        return shop;
    }

    public void setShop(Shop shop) {
        this.shop = shop;
    }

    public Role getRole() {
        return role;
    }

    public void setRole(Role role) {
        this.role = role;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        User user = (User) o;
        return Objects.equals(userId, user.userId)
                && Objects.equals(username, user.username)
                && Objects.equals(password, user.password)
                && Objects.equals(region, user.region)
                && Objects.equals(shop, user.shop)
                && Objects.equals(role, user.role);
    }

    @Override
    public int hashCode() {
        return Objects.hash(userId, username, password, region, shop, role);
    }

    @Override
    public String toString() {
        return "User{" +
                "userId=" + userId +
                ", username='" + username + '\'' +
                ", password='" + password + '\'' +
                ", region=" + region +
                ", shop=" + shop +
                ", role=" + role +
                '}';
    }
}
