package com.kamiljacko.restapi.user;


import com.kamiljacko.restapi.region.Region;
import com.kamiljacko.restapi.shop.Shop;
import com.kamiljacko.restapi.web.dto.UserDto;
import org.springframework.stereotype.Service;

import java.util.List;
import java.util.Objects;
import java.util.Optional;
import java.util.stream.Collectors;
import java.util.stream.StreamSupport;

@Service
public class UserServiceImpl implements UserService {

    private static final Integer DB_NULL_VALUE = null;

    private final UserRepository userRepository;

    public UserServiceImpl(final UserRepository userRepository) {
        this.userRepository = userRepository;
    }

    @Override
    public List<UserDto> getAllUsers() {
        final Iterable<User> allUsers = userRepository.findAll();
        return StreamSupport.stream(allUsers.spliterator(), false)
                .map(this::mapUserToDto)
                .collect(Collectors.toList());
    }

    @Override
    public UserDto addUser(final UserDto userDto) {
        final User User = mapDtoToEntity(userDto);
        final User savedUser = userRepository.save(User);
        return mapUserToDto(savedUser);
    }

    @Override
    public void updateUser(final UserDto userDto) {
        //user id in dto
        final Integer userId = userDto.getUserId();
        final Optional<User> user = userRepository.findById(userId);
        final UserDto retrievedUserDto = mapUserToDto(user.get());

        final User userToUpdate = updateUser(retrievedUserDto, userDto);

        userRepository.updateUser(
                userToUpdate.getUsername(),
                userToUpdate.getPassword(),
                userToUpdate.getRole().getRoleId(),
                userToUpdate.getRegion().getRegionId(),
                userToUpdate.getShop().getShopId(),
                userToUpdate.getUserId()
        );
    }

    @Override
    public void deleteUser(final Integer userId) {
        userRepository.deleteById(userId);
    }

    private User updateUser(final UserDto retrievedUserDto, final UserDto userDto) {
        final User user = new User(retrievedUserDto);
        if (Objects.nonNull(userDto.getUsername()) &&
                !retrievedUserDto.getUsername().equals(userDto.getUsername())) {
            user.setUsername(userDto.getUsername());
        }
        if (Objects.nonNull(userDto.getPassword()) &&
                !retrievedUserDto.getPassword().equals(userDto.getPassword())) {
            user.setPassword(userDto.getPassword());
        }
        if (Objects.nonNull(userDto.getRoleId()) &&
                !retrievedUserDto.getRoleId().equals(userDto.getRoleId())) {
            user.setRole(new Role(userDto.getRoleId()));
        }
        if (Objects.nonNull(userDto.getRegionId())) {
            user.setRegion(new Region(userDto.getRegionId()));
        }
        if (Objects.nonNull(userDto.getShopId())) {
            user.setShop(new Shop(userDto.getShopId()));
        }
        return user;
    }

    private UserDto mapUserToDto(final User user) {
        final UserDto userDto = new UserDto();
        userDto.setUserId(user.getUserId());
        userDto.setUsername(user.getUsername());
        userDto.setPassword(user.getPassword());
        userDto.setRoleId(user.getRole().getRoleId());
        userDto.setShopId(Objects.isNull(user.getShop()) ? DB_NULL_VALUE : user.getShop().getShopId());
        userDto.setRegionId(Objects.isNull(user.getRegion()) ? DB_NULL_VALUE : user.getRegion().getRegionId());
        return userDto;
    }

    private User mapDtoToEntity(final UserDto userDto) {
        final User user = new User();
        user.setUsername(userDto.getUsername());
        user.setPassword(userDto.getPassword());
        user.setRole(new Role(userDto.getRoleId()));
        user.setShop(Objects.isNull(userDto.getShopId()) ? null : new Shop(userDto.getShopId()));
        user.setRegion(Objects.isNull(userDto.getRegionId()) ? null : new Region(userDto.getRegionId()));
        return user;
    }
}