package com.kamiljacko.restapi.stock;


import com.kamiljacko.restapi.product.Product;
import com.kamiljacko.restapi.shop.Shop;
import com.kamiljacko.restapi.web.dto.ProductDto;
import com.kamiljacko.restapi.web.dto.ProductFromGivenShopDto;

import javax.persistence.Column;
import javax.persistence.EmbeddedId;
import javax.persistence.Entity;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.MapsId;
import javax.persistence.Table;
import java.util.Objects;

@Entity
@Table(name = "stocks")
public class Stock {

    @EmbeddedId
    private StockKey id;

    @ManyToOne
    @MapsId("productId")
    @JoinColumn(name = "productid")
    private Product product;

    @ManyToOne
    @MapsId("shopId")
    @JoinColumn(name = "shopid")
    private Shop shop;

    private Integer amount;
    private String price;

    @Column(name = "discount_price")
    private String discountPrice;

    @Column(name = "is_discounted")
    private Integer isDiscounted;

    Stock() {
    }

    public Stock(ProductFromGivenShopDto product) {
        this.product = new Product(product.getProductName(), product.getProductId());
        this.shop = new Shop(product.getShopId());
        this.amount = product.getAmount();
        this.price = product.getPrice();
        this.discountPrice = product.getDiscountPrice();
        this.isDiscounted = product.getIsDiscounted();
        this.id = new StockKey(product.getProductId(), shop.getShopId());
    }

    public Stock(ProductDto product, Integer shopId) {
        this.product = new Product(product.getProductId());
        this.shop = new Shop(shopId);
        this.amount = product.getAmount();
        this.price = product.getPrice();
        this.discountPrice = product.getDiscountPrice();
        this.isDiscounted = product.getIsDiscounted();
        this.id = new StockKey(product.getProductId(), shop.getShopId());
    }

    public StockKey getId() {
        return id;
    }

    public void setId(StockKey id) {
        this.id = id;
    }

    public Product getProduct() {
        return product;
    }

    public void setProduct(Product product) {
        this.product = product;
    }

    public Shop getShop() {
        return shop;
    }

    public void setShop(Shop shop) {
        this.shop = shop;
    }

    public Integer getAmount() {
        return amount;
    }

    public void setAmount(Integer amount) {
        this.amount = amount;
    }

    public String getPrice() {
        return price;
    }

    public void setPrice(String price) {
        this.price = price;
    }

    public String getDiscountPrice() {
        return discountPrice;
    }

    public void setDiscountPrice(String discountPrice) {
        this.discountPrice = discountPrice;
    }

    public Integer getIsDiscounted() {
        return isDiscounted;
    }

    public void setIsDiscounted(Integer isDiscounted) {
        this.isDiscounted = isDiscounted;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        Stock stock = (Stock) o;
        return Objects.equals(id, stock.id)
                && Objects.equals(product, stock.product)
                && Objects.equals(shop, stock.shop)
                && Objects.equals(amount, stock.amount)
                && Objects.equals(price, stock.price)
                && Objects.equals(discountPrice, stock.discountPrice)
                && Objects.equals(isDiscounted, stock.isDiscounted);
    }

    @Override
    public int hashCode() {
        return Objects.hash(id, product, shop, amount, price, discountPrice, isDiscounted);
    }

    @Override
    public String toString() {
        return "Stock{" +
                "id=" + id +
                ", product=" + product +
                ", shop=" + shop +
                ", amount=" + amount +
                ", price='" + price + '\'' +
                ", discountPrice='" + discountPrice + '\'' +
                ", isDiscounted=" + isDiscounted +
                '}';
    }
}
