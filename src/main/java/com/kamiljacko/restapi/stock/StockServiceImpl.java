package com.kamiljacko.restapi.stock;

import com.kamiljacko.restapi.product.Category;
import com.kamiljacko.restapi.product.CategoryRepository;
import com.kamiljacko.restapi.product.Product;
import com.kamiljacko.restapi.product.ProductRepository;
import com.kamiljacko.restapi.region.Region;
import com.kamiljacko.restapi.region.RegionRepository;
import com.kamiljacko.restapi.shop.ShopRepository;
import com.kamiljacko.restapi.web.dto.MinimalProductDto;
import com.kamiljacko.restapi.web.dto.ProductDto;
import com.kamiljacko.restapi.web.dto.ProductFromGivenShopDto;
import com.kamiljacko.restapi.web.dto.StockDto;
import org.springframework.stereotype.Service;

import java.util.List;
import java.util.Objects;
import java.util.Optional;
import java.util.stream.Collectors;
import java.util.stream.StreamSupport;

import static com.kamiljacko.restapi.CommonMapper.mapStockToDto;
import static com.kamiljacko.restapi.CommonMapper.mapToDto;

@Service
public class StockServiceImpl implements StockService {

    private final ProductRepository productRepository;
    private final StockRepository stockRepository;
    private final ShopRepository shopRepository;
    private final RegionRepository regionRepository;
    private final CategoryRepository categoryRepository;

    public StockServiceImpl(final ProductRepository productRepository,
                            final StockRepository stockRepository,
                            final ShopRepository shopRepository,
                            final RegionRepository regionRepository,
                            final CategoryRepository categoryRepository) {
        this.stockRepository = stockRepository;
        this.productRepository = productRepository;
        this.shopRepository = shopRepository;
        this.regionRepository = regionRepository;
        this.categoryRepository = categoryRepository;
    }

    @Override
    public List<MinimalProductDto> getAllProducts() {
        final Iterable<Product> allProducts = productRepository.findAll();
        return StreamSupport.stream(allProducts.spliterator(), false)
                .map(this::mapProductToDto)
                .collect(Collectors.toList());
    }

    @Override
    public StockDto addRegionalProductToGivenShop(final String shopName, final ProductDto productDto) {
        final Optional<Region> region = Optional.ofNullable(regionRepository.findRegionByRegionName(productDto.getRegionName()));
        final Integer regionId = region.map(Region::getRegionId).orElse(0);
        productDto.setRegionId(regionId);

        final Optional<Category> category = Optional.ofNullable(categoryRepository.findCategoryByCategoryName(productDto.getCategoryName()));
        final Integer categoryId = category.map(Category::getCategoryId).orElse(0);
        productDto.setCategoryId(categoryId);

        final Product regionalProduct = mapDtoToEntity(productDto);

        final Integer regionalProductId = productRepository.save(regionalProduct).getProductId();
        productDto.setProductId(regionalProductId);

        final Integer shopId = shopRepository.getShopIdByName(shopName);
        final Stock stock = new Stock(productDto, shopId);
        final Stock savedStock = stockRepository.save(stock);

        return mapStockToDto(savedStock);
    }

    /**
     * Method provides update functionality. Updating shop and region not provided.
     **/
    @Override
    public void updateProduct(final String shopName, final ProductDto updateDto) {
        //product id in dto
        final Integer productId = updateDto.getProductId();
        final ShopRepository.FullDao productFromGivenShop = shopRepository.getProductFromGivenShop(shopName, productId);

        if (Objects.nonNull(updateDto.getCategoryName())) {
            final Integer categoryId = categoryRepository.findCategoryByCategoryName(updateDto.getCategoryName()).getCategoryId();
            updateDto.setCategoryId(categoryId);
        }

        final ProductFromGivenShopDto product = mapToDto(productFromGivenShop);

        final Stock stockToUpdate = updateStock(product, updateDto);

        stockRepository.updateStock(
                stockToUpdate.getProduct().getName(),
                stockToUpdate.getProduct().getDescription(),
                stockToUpdate.getProduct().getImgUrl(),
                stockToUpdate.getProduct().getCategory().getCategoryId(),
                stockToUpdate.getAmount(),
                stockToUpdate.getPrice(),
                stockToUpdate.getDiscountPrice(),
                stockToUpdate.getIsDiscounted(), shopName, productId);
    }

    private Stock updateStock(final ProductFromGivenShopDto product, final ProductDto updateDto) {
        final Stock stockToSave = new Stock(product);
        final Product productToSave = updateProduct(product, updateDto);

        if (Objects.nonNull(updateDto.getAmount()) &&
                !product.getAmount().equals(updateDto.getAmount())) {
            stockToSave.setAmount(updateDto.getAmount());
        }
        if (Objects.nonNull(updateDto.getPrice()) &&
                !product.getPrice().equals(updateDto.getPrice())) {
            stockToSave.setPrice(updateDto.getPrice());
        }
        if (Objects.nonNull(updateDto.getDiscountPrice()) &&
                !product.getDiscountPrice().equals(updateDto.getDiscountPrice())) {
            stockToSave.setDiscountPrice(updateDto.getDiscountPrice());
        }
        if (Objects.nonNull(updateDto.getIsDiscounted()) &&
                !product.getIsDiscounted().equals(updateDto.getIsDiscounted())) {
            stockToSave.setIsDiscounted(updateDto.getIsDiscounted());
        }
        stockToSave.setProduct(productToSave);
        return stockToSave;
    }

    private Product updateProduct(final ProductFromGivenShopDto product, final ProductDto updateDto) {
        final Product productToSave = new Product(
                product.getProductName(),
                product.getProductId(),
                product.getProductDescription(),
                product.getImgUrl(),
                product.getProductCategoryId());
        if (Objects.nonNull(updateDto.getProductName()) &&
                !product.getProductName().equals(updateDto.getProductName())) {
            productToSave.setName(updateDto.getProductName());
        }
        if (Objects.nonNull(updateDto.getProductDescription()) &&
                !product.getProductDescription().equals(updateDto.getProductDescription())) {
            productToSave.setDescription(updateDto.getProductDescription());
        }
        if (Objects.nonNull(updateDto.getImgUrl()) &&
                !product.getImgUrl().equals(updateDto.getImgUrl())) {
            productToSave.setImgUrl(updateDto.getImgUrl());
        }
        if (Objects.nonNull(updateDto.getCategoryId()) &&
                !product.getProductCategoryId().equals(updateDto.getCategoryId())) {
            productToSave.setCategory(new Category(updateDto.getCategoryId()));
        }
        return productToSave;
    }

    private MinimalProductDto mapProductToDto(final Product product) {
        final MinimalProductDto productDto = new MinimalProductDto();
        productDto.setProductId(product.getProductId());
        productDto.setProductName(product.getName());
        return productDto;
    }

    private Product mapDtoToEntity(final ProductDto productDto) {
        final Product product = new Product();
        product.setName(productDto.getProductName());
        product.setRegion(new Region(productDto.getRegionId()));
        product.setCategory(new Category(productDto.getCategoryId()));
        product.setDescription(productDto.getProductDescription());
        product.setImgUrl(productDto.getImgUrl());
        return product;
    }
}
