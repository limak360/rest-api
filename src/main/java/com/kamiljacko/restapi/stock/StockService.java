package com.kamiljacko.restapi.stock;


import com.kamiljacko.restapi.web.dto.MinimalProductDto;
import com.kamiljacko.restapi.web.dto.StockDto;
import com.kamiljacko.restapi.web.dto.ProductDto;

import java.util.List;

public interface StockService {

    List<MinimalProductDto> getAllProducts();

    StockDto addRegionalProductToGivenShop(final String shopName, final ProductDto productDto);

    void updateProduct(final String shopName, final ProductDto updateDto);

}
