package com.kamiljacko.restapi.stock;

import org.springframework.data.jpa.repository.Modifying;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.CrudRepository;

import javax.transaction.Transactional;

@Transactional
public interface StockRepository extends CrudRepository<Stock, Integer> {
    @Modifying
    @Query(value = "UPDATE stocks st " +
            "JOIN shops s ON st.shopid = s.shopId " +
            "JOIN products p ON st.productid = p.productId " +
            "SET p.name=:productName, p.description=:productDescription, p.imgUrl=:imgUrl, p.categoryid=:categoryId, " +
            "st.amount=:amount, st.price=:price, st.discount_price=:discountPrice, st.is_discounted=:isDiscounted " +
            "WHERE s.name=:shopName AND p.productid=:productId", nativeQuery = true)
    void updateStock(final String productName, final String productDescription, final String imgUrl, final Integer categoryId,
                     final Integer amount, final String price, final String discountPrice, final Integer isDiscounted,
                     final String shopName, final Integer productId);
}
