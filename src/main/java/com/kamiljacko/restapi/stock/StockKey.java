package com.kamiljacko.restapi.stock;

import javax.persistence.Column;
import javax.persistence.Embeddable;
import java.io.Serializable;
import java.util.Objects;

@Embeddable
class StockKey implements Serializable {

    @Column(name = "productid")
    private Integer productId;

    @Column(name = "shopid")
    private Integer shopId;

    public StockKey() {
    }

    public StockKey(Integer productId, Integer shopId) {
        this.productId = productId;
        this.shopId = shopId;
    }

    public Integer getProductId() {
        return productId;
    }

    public void setProductId(Integer productId) {
        this.productId = productId;
    }

    public Integer getShopId() {
        return shopId;
    }

    public void setShopId(Integer shopId) {
        this.shopId = shopId;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        StockKey stockKey = (StockKey) o;
        return Objects.equals(productId, stockKey.productId)
                && Objects.equals(shopId, stockKey.shopId);
    }

    @Override
    public int hashCode() {
        return Objects.hash(productId, shopId);
    }

    @Override
    public String toString() {
        return "StockKey{" +
                "productId=" + productId +
                ", shopId=" + shopId +
                '}';
    }
}