package com.kamiljacko.restapi.region;

import com.kamiljacko.restapi.web.dto.RegionDto;

import java.util.List;

public interface RegionService {

    List<RegionDto> getAllRegions();

    RegionDto addRegion(final RegionDto regionDto);

    void deleteRegion(final String regionName);
}
