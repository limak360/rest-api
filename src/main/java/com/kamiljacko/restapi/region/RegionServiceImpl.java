package com.kamiljacko.restapi.region;


import com.kamiljacko.restapi.web.dto.RegionDto;
import org.springframework.stereotype.Service;

import java.util.List;
import java.util.stream.Collectors;
import java.util.stream.StreamSupport;

@Service
public class RegionServiceImpl implements RegionService {

    private final RegionRepository regionRepository;

    public RegionServiceImpl(final RegionRepository regionRepository) {
        this.regionRepository = regionRepository;
    }

    @Override
    public List<RegionDto> getAllRegions() {
        final Iterable<Region> allRegions = regionRepository.findAll();
        return StreamSupport.stream(allRegions.spliterator(), false)
                .map(this::mapRegionToDto)
                .collect(Collectors.toList());
    }

    @Override
    public RegionDto addRegion(final RegionDto regionDto) {
        final Region region = mapDtoToEntity(regionDto);
        final Region savedRegion = regionRepository.save(region);
        return mapRegionToDto(savedRegion);
    }

    @Override
    public void deleteRegion(final String regionName) {
        regionRepository.deleteRegion(regionName);
    }

    private RegionDto mapRegionToDto(final Region region) {
        final RegionDto regionDto = new RegionDto();
        regionDto.setId(region.getRegionId());
        regionDto.setName(region.getName());
        return regionDto;
    }

    private Region mapDtoToEntity(final RegionDto regionDto) {
        final Region region = new Region();
        region.setName(regionDto.getName());
        return region;
    }
}
