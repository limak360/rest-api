package com.kamiljacko.restapi.region;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Modifying;
import org.springframework.data.jpa.repository.Query;

import javax.transaction.Transactional;

@Transactional
public interface RegionRepository extends JpaRepository<Region, Integer> {

    @Query("FROM Region Where name=:regionName")
    Region findRegionByRegionName(final String regionName);

    @Modifying
    @Query(value = "Delete regions From regions Where name=:regionName", nativeQuery = true)
    void deleteRegion(final String regionName);
}
