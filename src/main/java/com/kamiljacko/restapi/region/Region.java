package com.kamiljacko.restapi.region;

import com.kamiljacko.restapi.product.Product;
import com.kamiljacko.restapi.shop.Shop;
import com.kamiljacko.restapi.user.User;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.OneToMany;
import javax.persistence.Table;
import java.util.List;
import java.util.Objects;

@Entity
@Table(name = "regions")
public class Region {

    @Id
    @Column(name = "regionid")
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Integer regionId;

    private String name;

    @OneToMany(mappedBy = "region")
    private List<Product> products;

    @OneToMany(mappedBy = "region")
    private List<Shop> shops;

    @OneToMany(mappedBy = "region")
    private List<User> user;

    public Region() {
    }

    public Region(Integer regionId) {
        this.regionId = regionId;
    }

    public Integer getRegionId() {
        return regionId;
    }

    public void setRegionId(Integer regionId) {
        this.regionId = regionId;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public List<Product> getProducts() {
        return products;
    }

    public void setProducts(List<Product> products) {
        this.products = products;
    }

    public List<Shop> getShops() {
        return shops;
    }

    public void setShops(List<Shop> shops) {
        this.shops = shops;
    }

    public List<User> getUser() {
        return user;
    }

    public void setUser(List<User> user) {
        this.user = user;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        Region region = (Region) o;
        return Objects.equals(regionId, region.regionId)
                && Objects.equals(name, region.name)
                && Objects.equals(products, region.products)
                && Objects.equals(shops, region.shops)
                && Objects.equals(user, region.user);
    }

    @Override
    public int hashCode() {
        return Objects.hash(regionId, name, products, shops, user);
    }

    @Override
    public String toString() {
        return "Region{" +
                "regionId=" + regionId +
                ", name='" + name + '\'' +
                ", products=" + products +
                ", shops=" + shops +
                ", user=" + user +
                '}';
    }
}
