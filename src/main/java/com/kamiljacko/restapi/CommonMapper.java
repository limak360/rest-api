package com.kamiljacko.restapi;

import com.kamiljacko.restapi.shop.ShopRepository;
import com.kamiljacko.restapi.stock.Stock;
import com.kamiljacko.restapi.web.dto.ProductFromGivenShopDto;
import com.kamiljacko.restapi.web.dto.StockDto;

public class CommonMapper {
    public static StockDto mapStockToDto(final Stock stock) {
        final StockDto stockDto = new StockDto();
        stockDto.setProductId(stock.getProduct().getProductId());
        stockDto.setShopId(stock.getShop().getShopId());
        stockDto.setAmount(stock.getAmount());
        stockDto.setPrice(stock.getPrice());
        stockDto.setDiscountPrice(stock.getDiscountPrice());
        stockDto.setIsDiscounted(stock.getIsDiscounted());
        return stockDto;
    }

    public static ProductFromGivenShopDto mapToDto(final ShopRepository.FullDao dao) {
        final ProductFromGivenShopDto productFromGivenShopDto = new ProductFromGivenShopDto();
        productFromGivenShopDto.setShopId(dao.getShopId());
        productFromGivenShopDto.setShopRegionId(dao.getShopRegionId().getRegionId());
        productFromGivenShopDto.setProductId(dao.getProduct().getProductId());
        productFromGivenShopDto.setName(dao.getName());
        productFromGivenShopDto.setImgUrl(dao.getImgUrl());
        productFromGivenShopDto.setAmount(dao.getAmount());
        productFromGivenShopDto.setPrice(dao.getPrice());
        productFromGivenShopDto.setDiscountPrice(dao.getDiscountPrice());
        productFromGivenShopDto.setIsDiscounted(dao.getIsDiscounted());
        productFromGivenShopDto.setProductName(dao.getProductName());
        productFromGivenShopDto.setProductRegionId(dao.getProductRegionId().getRegionId());
        productFromGivenShopDto.setProductCategoryId(dao.getProductCategoryId().getCategoryId());
        productFromGivenShopDto.setProductDescription(dao.getProductDescription());
        productFromGivenShopDto.setImgUrl(dao.getImgUrl());
        return productFromGivenShopDto;
    }
}